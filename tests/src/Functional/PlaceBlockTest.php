<?php

namespace Drupal\Tests\simple_klaro\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test for place klaro block.
 *
 * @package Drupal\Tests\simple_klaro\Functional
 * @group simple_klaro
 */
class PlaceBlockTest extends BrowserTestBase {

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'block',
    'simple_klaro',
  ];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = "olivero";

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $config = $this->config('simple_klaro.settings');
    $config->set('library', 'klaro_cdn');
    $config->save();
    $this->drupalPlaceBlock('simple_klaro_preferences_dialog', ['region' => 'footer_bottom']);
  }

  /**
   * Test if we can place the block.
   */
  public function testPlace() {
    $config = $this->config('simple_klaro.settings');
    $this->drupalGet("<front>");
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseContains($config->get('preferences'));
    $this->assertSession()->responseContains('https://cdn.kiprotect.com/klaro/');
  }

  /**
   * Test if preference title can changed.
   */
  public function testPreferenceTitle() {
    $config = $this->config('simple_klaro.settings');
    $config->set('preferences', 'unique_title_to_set');
    $config->save();
    $this->drupalPlaceBlock('simple_klaro_preferences_dialog', ['region' => 'footer_bottom']);
    $this->drupalGet(Url::fromRoute("<front>"));
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseContains('unique_title_to_set');
  }

  /**
   * Test if klaro displayed on excluded paths.
   */
  public function testExcludePaths() {
    $config = $this->config('simple_klaro.settings');
    $config->set('exclude_paths', "<front>");
    $config->save();
    $this->drupalPlaceBlock('simple_klaro_preferences_dialog', ['region' => 'footer_bottom']);
    $this->drupalGet("<front>");
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseNotContains('Cookie preferences');
    $this->assertSession()->responseNotContains('https://cdn.kiprotect.com/klaro/');
  }

  /**
   * Test if klaro disabled.
   */
  public function testDisabledKlaro() {
    $this->drupalPlaceBlock('simple_klaro_preferences_dialog', ['region' => 'footer_bottom']);
    $config = $this->config('simple_klaro.settings');
    $config->set('enabled', FALSE);
    $config->save();
    $this->drupalGet(Url::fromRoute("<front>"));
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseNotContains('Cookie preferences');
    $this->assertSession()->responseNotContains('https://cdn.kiprotect.com/klaro/');
  }

}
