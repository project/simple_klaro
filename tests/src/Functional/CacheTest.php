<?php

namespace Drupal\Tests\simple_klaro\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\system\Functional\Cache\AssertPageCacheContextsAndTagsTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test for place klaro block.
 *
 * @package Drupal\Tests\simple_klaro\Functional
 * @group simple_klaro
 */
class CacheTest extends BrowserTestBase {

  use AssertPageCacheContextsAndTagsTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['simple_klaro', 'block', 'page_cache'];

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = "olivero";

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $config = $this->config('simple_klaro.settings');
    $config->set('library', 'klaro_cdn');
    $config->save();
    $this->drupalPlaceBlock('simple_klaro_preferences_dialog', ['region' => 'footer_bottom']);
  }

  /**
   * Test if preference title can changed.
   */
  public function testCachePreferenceTitle() {
    $this->drupalGet(Url::fromRoute("<front>"));
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseContains('Cookie preferences');
    $config = $this->config('simple_klaro.settings');
    $config->set('preferences', 'unique_title_to_set');
    $config->save();
    $this->drupalGet(Url::fromRoute("<front>"));
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseContains('unique_title_to_set');
  }

  /**
   * Test if preference title can changed.
   */
  public function testCacheBlockVisibility() {
    $this->drupalGet(Url::fromRoute("<front>"));
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseContains('Cookie preferences');
    $config = $this->config('simple_klaro.settings');
    $config->set('exclude_paths', '<front>');
    $config->save();
    $this->drupalGet(Url::fromRoute("<front>"));
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseNotContains('Cookie preferences');
  }

  /**
   * Test if preference title can changed.
   */
  public function testJsLoading() {
    $this->drupalGet(Url::fromRoute("<front>"));
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseContains('https://cdn.kiprotect.com/klaro/');
    $config = $this->config('simple_klaro.settings');
    $config->set('exclude_paths', '<front>');
    $config->save();
    $this->drupalGet(Url::fromRoute("<front>"));
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
  }

}
