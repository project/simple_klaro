((Drupal, once) => {
  Drupal.behaviors.klaroEditor = {
    attach(context) {
      const initEditor = () => {
        once('klaro-editor', '[data-klaro-editor]', context).forEach(
          (textarea) => {
            const editDiv = document.createElement('div');
            textarea.classList.add('visually-hidden');
            textarea.parentNode.insertBefore(editDiv, textarea);
            editDiv.style.fontSize = 18;

            // Init ace editor.
            ace.config.set(
              'basePath',
              'https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.11/',
            );
            const editor = ace.edit(editDiv);
            editor.session.setValue(textarea.value);
            editor.session.setMode('ace/mode/json');
            editor.session.setTabSize(2);
            editor.setTheme('ace/theme/chrome');
            editor.setOptions({
              minLines: 3,
              maxLines: 20,
            });

            // Update Drupal textarea value.
            editor.getSession().on('change', () => {
              textarea.value = editor.getSession().getValue();
            });
          },
        );
      };

      // Initialize editor.
      if (typeof ace !== 'undefined') {
        initEditor();
      }
    },
  };
})(Drupal, once);
