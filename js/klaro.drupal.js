((Drupal, drupalSettings, once) => {
  Drupal.behaviors.klaro = {
    attach: function attach(context) {
      if (drupalSettings.klaroConfig) {
        // Open preferences
        once('klaro-init', 'body', context).forEach(() => {
          const preferences = context.querySelectorAll(
            '#klaro-preferences, .klaro-preferences',
          );
          if (drupalSettings.klaroConfig.services !== undefined) {
            drupalSettings.klaroConfig.services.forEach((value, index) => {
              if (
                drupalSettings.klaroConfig.services[index].callback !==
                undefined
              ) {
                drupalSettings.klaroConfig.services[index].callback =
                  new Function(
                    `return ${drupalSettings.klaroConfig.services[index].callback}`,
                  )();
              }
            });
          }

          if (preferences !== null) {
            preferences.forEach((button) => {
              button.addEventListener('click', (e) => {
                e.preventDefault();
                klaro.show(drupalSettings.klaroConfig);
              });
            });
          }
          // Render klaro
          klaro.render(drupalSettings.klaroConfig);
        });
      }
    },
  };
})(Drupal, drupalSettings, once);
