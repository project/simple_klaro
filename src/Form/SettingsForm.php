<?php

namespace Drupal\simple_klaro\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * General settings form for the Simple Klaro.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Klaro settings for the Drupal integration.
   */
  const SETTINGS = 'simple_klaro.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_klaro_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Simple Klaro'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Klaro Config'),
      '#default_value' => $config->get('config'),
      '#description' => $this->t('Enter the Klaro config variable content as JSON. An example of config file can be found <a href="@example" target="_blank">here</a>. The documentation can be found <a href="@documentation" target="_blank">here</a>.',
        [
          '@example' => 'https://heyklaro.com/docs/integration/annotated-configuration',
          '@documentation' => 'https://heyklaro.com/docs/',
        ]
      ),
      '#required' => TRUE,
      '#rows' => 20,
      '#attributes' => ['data-klaro-editor' => 'true'],
    ];

    $form['preferences'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Preferences label'),
      '#default_value' => $config->get('preferences') ? $config->get('preferences') : $this->t('Cookie preferences'),
      '#description' => $this->t('Link label to open the preferences dialog after the preferences have been saved.'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
    ];

    $form['library'] = [
      '#type' => 'select',
      '#title' => $this->t('Klaro library'),
      '#description' => $this->t('Select the Klaro library to be included. If you want to provide your own styles use the no-css library.'),
      '#default_value' => $config->get('library') ? $config->get('library') : 'klaro',
      '#options' => [
        'klaro' => $this->t('Klaro (default)'),
        'klaro_no_css' => $this->t('Klaro (no css)'),
        'klaro_no_translations' => $this->t('Klaro (no translations)'),
        'klaro_no_translations_no_css' => $this->t('Klaro (no translations and no css)'),
        'klaro_cdn' => $this->t('Klaro via CDN'),
        'klaro_cdn_no_css' => $this->t('Klaro (no css) via CDN'),
        'klaro_cdn_no_translations' => $this->t('Klaro (no translations) via CDN'),
        'klaro_cdn_no_translations_no_css' => $this->t('Klaro (no translations and no css) via CDN'),
      ],
      '#required' => TRUE,
    ];

    $form['exclude_paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exclude paths'),
      '#default_value' => !empty($config->get('exclude_paths')) ? $config->get('exclude_paths') : '',
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
        '%blog' => '/blog',
        '%blog-wildcard' => '/blog/*',
        '%front' => '<front>',
      ]),
    ];

    $form['#attached']['library'][] = 'simple_klaro/klaro_editor';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    Json::decode($form_state->getValue('config'));
    if (json_last_error() !== JSON_ERROR_NONE) {
      $form_state->setErrorByName('config', $this->t('The data is not valid JSON.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('config', $form_state->getValue('config'))
      ->set('preferences', $form_state->getValue('preferences'))
      ->set('library', $form_state->getValue('library'))
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('exclude_paths', $form_state->getValue('exclude_paths'))
      ->save();

    parent::submitForm($form, $form_state);

    drupal_flush_all_caches();
  }

}
