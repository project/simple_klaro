<?php

namespace Drupal\simple_klaro\Plugin\Block;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to open the preferences dialog for Simple Klaro.
 *
 * @Block(
 *   id = "simple_klaro_preferences_dialog",
 *   admin_label = @Translation("Simple Klaro preferences dialog"),
 * )
 */
class PreferencesDialog extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionPluginManager;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs an PreferencesDialog object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The plugin manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, PluginManagerInterface $plugin_manager, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->conditionPluginManager = $plugin_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('plugin.manager.condition'),
      $container->get("renderer"),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('simple_klaro.settings');
    $build = [
      '#markup' => '<a href="#" id="klaro-preferences">' . $config->get('preferences') . '</a>',
    ];
    $this->renderer->addCacheableDependency($build, $config);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $config = $this->configFactory->get('simple_klaro.settings');
    $klaro_enabled = $config->get('enabled');
    if (!$klaro_enabled) {
      return AccessResult::forbidden();
    }
    if ($account->hasPermission('bypass simple klaro')) {
      return AccessResult::forbidden();
    }
    if (empty($config->get('exclude_paths'))) {
      return parent::blockAccess($account);
    }
    $condition = $this->conditionPluginManager->createInstance('request_path');
    $condition->setConfig('pages', $config->get('exclude_paths'));
    if ($condition->execute()) {
      return AccessResult::forbidden();
    }

    return parent::blockAccess($account);
  }

}
