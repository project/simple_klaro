# Simple Klaro

This module provides lightweight implementation of [*Klaro!  A Simple
Consent Manager*](https://github.com/KIProtect/klaro).

Once the module is enabled and configured it is automatically placed on the
website.

If users with certain roles should not see the consent manager, the
permission *"Bypass simple klaro"* can be set.

The configuration settings of the module are here:
`/admin/config/system/simple-klaro`.

Klaro can be uses with standard styles or without CSS.

The module provides a block with a link to open the preferences dialog after
the settings have been saved.

Example Json for klaro config setting:

```json
{
   "elementID":"klaro",
   "storageMethod":"localStorage",
   "cookieName":"klaro",
   "cookieExpiresAfterDays":365,
   "groupByPurpose": true,
   "privacyPolicy":"https://example.com/privacy-policy",
   "default":false,
   "mustConsent":false,
   "acceptAll":true,
   "hideDeclineAll":false,
   "translations":{
      "de":{
         "consentModal":{
            "description":"Wir verwenden Cookies."
         },
         "googleAnalytics":{
            "description":"Analyse des Nutzerverhaltens auf der Website"
         },
         "googleFonts":{
            "description":"Web-Schriftarten von Google gehostet"
         },
         "youtube":{
            "description":"Versucht, die Benutzerbandbreite auf Seiten mit integrierten YouTube-Videos zu schätzen."
         },
         "purposes":{
            "analytics":"Analyse von Benutzerverhalten",
            "security":"Sicherheit",
            "styling":"Styling"
         }
      },
      "en":{
         "consentModal":{
            "description":"We use cookies."
         },
         "googleAnalytics":{
            "description":"Tracking of user behavior on the website"
         },
         "googleFonts":{
            "description":"Web fonts hosted by Google"
         },
         "youtube":{
            "description":"Analyses the YouTube watching behaviour"
         },
         "purposes":{
            "analytics":"Analytics",
            "security":"Security",
            "styling":"Styling"
         }
      }
   },
   "services":[
      {
         "name":"googleAnalytics",
         "title":"Google Analytics",
         "purposes":[
            "analytics"
         ],
         "default":true,
         "cookies":[
            "/^_ga.*$/",
            "/^_gid.*$/"
         ]
      },
      {
         "name":"googleFonts",
         "title":"Google Fonts",
         "purposes":[
            "styling"
         ],
         "default":true,
         "required":true
      },
      {
         "name":"youtube",
         "title":"YouTube",
         "purposes":[
           "analytics"
         ],
         "default":false,
         "cookies":[
           "/^VISITOR_INFO1_LIVE*$/",
           "/^YSC*$/"
         ],
         "contextualConsentOnly": false
      }
   ]
}
```
